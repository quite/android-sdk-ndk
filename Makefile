img=docker.io/quite/android-sdk-ndk:7

lint:
	hadolint --ignore DL3008 --ignore DL4006 Dockerfile

build:
	podman build -t $(img) .

push:
	podman push $(img)
