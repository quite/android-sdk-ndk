FROM ubuntu:22.04
LABEL maintainer="Daniel Lublin <daniel@lublin.se>"

ENV ANDROID_SDK_ROOT "/sdk"
# For the NDK, packages.txt lists the version that we need. This gets
# installed in $ANDROID_SDK_ROOT/ndk/maj.min.build and is found
# automatically since the same android.ndkVersion is set in the
# build.grade that needs the NDK.

ENV PATH "$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update && apt-get -y upgrade \
    && apt-get install -y --no-install-recommends \
               build-essential \
               bzip2 \
               curl \
               file \
               git \
               locales \
               openjdk-19-jdk-headless \
               unzip \
    && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US.UTF-8
ENV LANG="en_US.UTF-8" LANGUAGE="en_US:en" LC_ALL="en_US.UTF-8"

RUN rm -rf /etc/ssl/certs/java/cacerts \
    && /var/lib/dpkg/info/ca-certificates-java.postinst configure

RUN curl -o /cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip \
    && mkdir -p $ANDROID_SDK_ROOT/cmdline-tools \
    && unzip -d $ANDROID_SDK_ROOT/cmdline-tools /cmdline-tools.zip \
    && rm -rf /cmdline-tools.zip \
    && mv $ANDROID_SDK_ROOT/cmdline-tools/cmdline-tools $ANDROID_SDK_ROOT/cmdline-tools/latest

RUN mkdir -p $ANDROID_SDK_ROOT/licenses \
    && printf >$ANDROID_SDK_ROOT/licenses/android-sdk-license "8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e\n24333f8a63b6825ea9c5514f83c2829b004d1fee\n" \
    && printf >$ANDROID_SDK_ROOT/licenses/android-sdk-preview-license "84831b9409646a918e30573bab4c9c91346d8abd\n504667f4c0de7af1a06de9f4b1727b84351f2910\n"

RUN yes | sdkmanager --update

COPY packages.txt /sdk
RUN sdkmanager --package_file=/sdk/packages.txt

RUN yes | sdkmanager --licenses
